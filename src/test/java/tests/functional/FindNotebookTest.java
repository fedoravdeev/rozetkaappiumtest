package tests.functional;

import core.BaseTest;
import org.junit.Test;
import pages.CatalogPage;
import pages.MainPage;

public class FindNotebookTest extends BaseTest {

    private MainPage page;
    private CatalogPage catalogPage;

    @Test
    public void findBestAsusNotebook(){
        page = new MainPage(driver);
        page.swipeLeftAndCloseLeftMenu(page)
                .clickOnCatalogButton()
                .clickOnSearchField();
    }

    @Test
    public void findFirstAsusNotebook()  {
        page = new MainPage(driver);
        page.swipeLeftAndCloseLeftMenu(page)
                .clickOnCatalogButton()
                .clickOnFirstLeftProduct()
                .swipeLeftAndCloseLeftMenu(page);
    }


    @Test
    public void addNotebookToCart(){
        catalogPage = new CatalogPage(driver);
        catalogPage.clickEnter()
                .clickToCatalog()
                .clickMenuNotebook()
                .clickMenuNotebookStep1()
                .clickMenuAll()
                .chooseAsusNotebook()
                .addToCart()
                ;
    }
}

