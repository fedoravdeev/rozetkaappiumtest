package tests.functional;

import pages.ComparePage;
import org.junit.Test;
import core.BaseTest;

public class CompareTest extends BaseTest {

    private ComparePage comparePage;

    @Test
    public void searchPageTest(){
        comparePage = new ComparePage(driver);
        comparePage.clickRozetkaBtn()
                .clickLaptops()
                .clickPrestigioItem()
                .clickCmpFirstBtn()
                .clickBackBtn()
                .clickAsusItem()
                .clickCmpSecondBtn()
                .clickMenu()
                .scrollHorizontal()
                .clickCompareMenuItem()
                .clickElementsCompare()
                .clickDifference()
                .clickMoreBtn()
                .clickLenovoPadItem()
                .clickCmpLenovoPad()
                .clickMenu()
                .scrollVertical()
                .clickCompareMenuItem()
                .clickElementsCompare()
                .verifyLeftElement()
                .verifyMiddleElement()
                .scrollHorizontal()
                .verifyRightElement();
    }
}
