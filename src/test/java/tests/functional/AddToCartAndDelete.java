package tests.functional;

import pages.MainPage;
import org.junit.Test;
import core.BaseTest;

public class AddToCartAndDelete extends BaseTest {

    private MainPage page;

    @Test
    public void test(){
        page = new MainPage(driver);
        page.swipeLeftAndCloseLeftMenu(page)
                .clickOnFirstLeftProduct()
                .clickOnBuyButtonOnProductPage()
                .clickAndMoveFromAuthPAgeToCart()
                .verifyAnyProductInCart()
                .clickOptionProductInCart()
                .clickDeleteButtonInOption()
                .verifyAnyProductNotInCart();
    }
}
