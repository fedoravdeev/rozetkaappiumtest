package tests.functional;

import core.BaseTest;
import core.CredentialTest;
import org.junit.Test;
import pages.PersonalAccountPage;

public class PersonalAccountTest extends BaseTest {

    private PersonalAccountPage page;

    @Test
    public void test() {
        CredentialTest credentialTest = new CredentialTest();
        credentialTest.init();
        page = new PersonalAccountPage(driver);
        page.swipeToBottom(page)
                .clickPersonalAccountMenu()
                .fillLoginField(credentialTest.getLogin())
                .fillPasswordField(credentialTest.getPassword())
                .hideKeyboard()
                .clickSignInBtn()
                .clickEditBtn()
                .swipeToBottom(page)
                .clickYogaCheckbox()
                .clickSubmitBtn()
                .verifyYogaInfo();
    }
}
