package tests.functional;

import core.BaseTest;
import org.junit.Test;
import pages.MainPage;

public class BuyProductFromWishList extends BaseTest {

    private MainPage page;

    @Test
    public void test(){
        page = new MainPage(driver);
        page.swipeLeftAndCloseLeftMenu(page)
                .clickOnFirstLeftProduct()
                .clickOnAddToWishListButton()
                .clickOnLeftMenuOpen()
                .clickOnWishListInLeftMenu()
                .verifyAnyProductInWishList()
                .clickOnProductFirstInWishList()
                .clickOnBuyButtonOnProductPage()
                .clickAndMoveFromAuthPAgeToCart()
                .verifyAnyProductInCart()
                .clickOptionProductInCart()
                .clickDeleteButtonInOption()
                .verifyAnyProductNotInCart();
    }
}