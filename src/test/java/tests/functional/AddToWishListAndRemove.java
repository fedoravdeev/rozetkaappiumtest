package tests.functional;

import pages.MainPage;
import org.junit.Test;
import core.BaseTest;

public class AddToWishListAndRemove extends BaseTest {

    private MainPage page;

    @Test
    public void test(){
        page = new MainPage(driver);
        page.swipeLeftAndCloseLeftMenu(page)
                .clickOnFirstLeftProduct()
                .clickOnAddToWishListButton()
                .clickOnLeftMenuOpen()
                .clickOnWishListInLeftMenu()
                .verifyAnyProductInWishList()
                .clickOnOptionProductinWishList()
                .clickOnDeleteButtoninOptionProductWishList()
                .verifyAnyProductNotInWishList();
    }
}
