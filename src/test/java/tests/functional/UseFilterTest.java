package tests.functional;

import core.BaseTest;
import core.CredentialTest;
import org.junit.Test;
import pages.ProductPage;

public class UseFilterTest extends BaseTest {

    private ProductPage productPage;

    @Test
    public void test() {
        CredentialTest credentialTest = new CredentialTest();
        credentialTest.init();
        productPage = new ProductPage(driver);
        productPage.clickMenuItemHeader()
                .clickCatalogBtn()
                .clickMenuLaptopesAndComputers()
                .clickMenuTablets()
                .clickMenuBudgetTablets()
                .clickFilterBtn()
                .clickUsedItemsCheckbox()
                .clickFilterApplyBtn()
                .clickSortBtn()
                .clickFirstRadioBtn()
                .clickDisplayTypeBtn()
                .clickFirstListProduct()
                .clickKeepTrackPrice()
                .fillLoginField(credentialTest.getLogin())
                .fillPasswordField(credentialTest.getPassword())
                .hideKeyboard()
                .clickSignInBtn()
                .clickKeepTrackPrice()
                .verifyProductInWaitList();
    }

}