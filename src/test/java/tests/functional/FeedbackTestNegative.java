package tests.functional;

import core.BaseTest;
import org.junit.Test;
import pages.FeedbackPage;

public class FeedbackTestNegative extends BaseTest {

    private FeedbackPage page;

    @Test
    public void test() {
        page = new FeedbackPage(driver);
        page.swipeToBottom(page)
                .clickFeedbackMenu()
                .clickSecondRadioBtn()
                .clickOtherQuestsRadioBtn()
                .fillMessageField("1")
                .swipeToBottom(page)
                .fillNameField("23")
                .fillEmailField("56")
                .hideKeyboard()
                .clickSendBtn()
                .verifyMessageSuccess();
    }
}
