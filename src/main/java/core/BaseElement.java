package core;

import exception.MobileException;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;

public class BaseElement {
    private static final Logger logger = LogManager.getRootLogger();
    private MobileElement mobileElement;

    protected Driver driver;

    public BaseElement(WebElement mobileElement) {

        if (!(mobileElement instanceof MobileElement)) {
            logger.error("Web Element cannot be defined as a Mobile Element." + mobileElement);
            throw new MobileException("Web Element cannot be defined as a Mobile Element.");
        }
        this.mobileElement = (MobileElement) mobileElement;
        logger.info("Create mobileElement = " + mobileElement);
    }

    public void click() {
        try {
            mobileElement.click();
            logger.info("Click mobileElement");
        } catch (ElementNotInteractableException e) {
            logger.error("ElementNotInteractableException." + e.getMessage(), e);
        } catch (NoSuchElementException e) {
            logger.error("NoSuchElementException." + e.getMessage(), e);
        } catch (Exception e) {
            logger.error("Exception." + e.getMessage(), e);
            e.printStackTrace();
        }
    }

    public boolean isExists() {
        return (mobileElement != null && mobileElement.isDisplayed());
    }

    public void sendKeys(String s) {
        logger.info("Send Keys as '" + s + "' " + mobileElement.toString());
        mobileElement.sendKeys(s);
    }

    public void swipeLeftNavigationDrawer(By el1, By el2) {
        TouchAction act = new TouchAction(driver.getDriver());
        Point el1coordinates = driver.getDriver().findElement(el1).getLocation();
        Point el2coordinates = driver.getDriver().findElement(el2).getLocation();
        act.press(PointOption.point(el1coordinates.x, el1coordinates.y))
                .moveTo(PointOption.point((el2coordinates.x - el1coordinates.x), (el2coordinates.y - el1coordinates.y)))
                .release().perform();
    }

    public String getText() {
        return mobileElement.getText();
    }
}
