package core;

import exception.MobileException;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;

import static java.time.Duration.ofMillis;

public class BasePage {
    protected Driver driver;
    protected static final Logger logger = LogManager.getRootLogger();
    private final int TWO_SECONDS = 2000;

    public BasePage(Driver driver) {
        this.driver = driver;
    }

    public <T extends BasePage> T swipeLeftAndCloseLeftMenu(T type) {
        wait(2000);
        horizontalSwipeByPercentage(0.90, 0.10, 0.50);
        wait(2000);
        return type;
    }

    public void wait(int timeout) {
        try {
            Thread.sleep(timeout);
            logger.info("Wait " + timeout + " seconds");
        } catch (Exception e) {
            logger.error("Error wait " + timeout + " seconds", e);
            throw new MobileException("Error wait ", e);
        }
    }

    public BaseElement getElementBy(By elementBy) {
        try {
            logger.info("Create BaseElement, find " + elementBy);
            return new BaseElement(driver.getDriver().findElement(elementBy));
        } catch (NoSuchElementException e) {
            logger.error("Element not found", e);
            throw new MobileException("Element not found");
        } catch (Exception e) {
            logger.error("Exception: getElementBy is not define.", e);
            throw new MobileException("Exception: getElementBy is not define. ", e);
        }
    }

    public void horizontalSwipeByPercentage(double startPercentage, double endPercentage, double anchorPercentage) {
        wait(2000);
        Dimension size = driver.getDriver().manage().window().getSize();
        logger.info("size.height = " + size.height);
        logger.info("size.width = " + size.width);
        logger.info("anchorPercentage = " + anchorPercentage);

        int anchor = (int) (size.height * anchorPercentage);
        int startPoint = (int) (size.width * startPercentage);
        int endPoint = (int) (size.width * endPercentage);

        logger.info("startPoint = " + startPoint);
        logger.info("endPoint = " + endPoint);
        logger.info("anchor = " + anchor);

        try {
            new TouchAction(driver.getDriver())
                    .press(PointOption.point(startPoint, anchor))
                    .waitAction(WaitOptions.waitOptions(ofMillis(TWO_SECONDS)))
                    .moveTo(PointOption.point(endPoint, anchor))
                    .release()
                    .perform();
        } catch (WebDriverException e) {
            logger.error("WebDriverException" + e.getMessage(), e);
            throw new MobileException("WebDriverException");
        }


    }

    public void verticalSwipeByPercentages(double startPercentage, double endPercentage, double anchorPercentage) {
        Dimension size = driver.getDriver().manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * endPercentage);

        logger.info("Vertical Swipe By Percentage");
        logger.info("startPoint " + startPoint);
        logger.info("endPoint " + endPoint);

        new TouchAction(driver.getDriver())
                .press(PointOption.point(anchor, startPoint))
                .waitAction(WaitOptions.waitOptions(ofMillis(1000)))
                .moveTo(PointOption.point(anchor, endPoint))
                .release().perform();
    }

    public <T extends BasePage> T swipeToBottom(T type) {
        wait(2000);
        Dimension dim = driver.getDriver().manage().window().getSize();
        int height = dim.getHeight();
        int width = dim.getWidth();
        int x = width / 2;
        int top_y = (int) (height * 0.80);
        int bottom_y = (int) (height * 0.20);
        TouchAction ts = new TouchAction(driver.getDriver());
        ts.press(PointOption.point(x, top_y)).moveTo(PointOption.point(x, bottom_y)).release().perform();
        return type;
    }

    public void swipeToBottom() {
        wait(2000);
        Dimension dim = driver.getDriver().manage().window().getSize();
        int height = dim.getHeight();
        int width = dim.getWidth();
        int x = width / 2;
        int top_y = (int) (height * 0.80);
        int bottom_y = (int) (height * 0.20);
        TouchAction ts = new TouchAction(driver.getDriver());
        ts.press(PointOption.point(x, top_y)).moveTo(PointOption.point(x, bottom_y)).release().perform();
    }

}
