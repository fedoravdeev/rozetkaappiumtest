package core;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Driver {
    private AppiumDriver driver;

    public Driver(URL url, DesiredCapabilities cap) {
        this.driver = new AppiumDriver(url, cap);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public AppiumDriver getDriver() {
        return this.driver;
    }

    public void quit() {
        driver.resetApp();
        driver.quit();
    }

}
