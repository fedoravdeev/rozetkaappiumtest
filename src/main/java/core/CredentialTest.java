package core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CredentialTest {
    private String login;
    private String password;

    public CredentialTest() {
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void init(){
        InputStream inputStream = BaseTest.class.getClassLoader().getResourceAsStream("cred.test.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setLogin(properties.getProperty("login"));
        setPassword(properties.getProperty("password"));
    }

}
