package core;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class BaseTest {

    private static final Logger logger = Logger.getRootLogger();

    protected Driver driver;

    @Before
    public void setupDriver() {

        logger.info("setupDriver inside initDriver method");
        DesiredCapabilities cap = new DesiredCapabilities();

        Set<String> devProperties = new HashSet<>();
        devProperties.add("deviceName");
        devProperties.add("platformName");
        devProperties.add("appPackage");
        devProperties.add("appActivity");

        try {
            String name = "appium.cap." + InetAddress.getLocalHost().getHostName() + ".properties";
            InputStream input = BaseTest.class.getClassLoader().getResourceAsStream(name);
            Properties prop = new Properties();
            prop.load(input);

            for (String s : devProperties) {
                cap.setCapability(s, prop.getProperty(s));
            }
            driver = new Driver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
            driver.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        } catch (IOException e) {
            logger.error("Can't find properties", e);
        }
    }

    @After
    public void tearDown() {
        logger.info("Driver quit");
        driver.quit();
    }

}
