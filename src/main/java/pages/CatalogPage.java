package pages;

import core.BasePage;
import core.Driver;
import org.openqa.selenium.By;

public class CatalogPage extends BasePage {

    private By mainSplash = By.id("ua.com.rozetka.shop:id/main_f_splash");
    private By buttonCatalog = By.id("ua.com.rozetka.shop:id/main_tv_to_catalog");
    private By imagevView = By.id("android.widget.ImageView");
    private By iv_toolbar_cart = By.id("ua.com.rozetka.shop:id/iv_toolbar_cart");
    private By smileRozetka = By.xpath("//android.widget.ImageButton[@content-desc='Перейти вверх']");
    private By mainBtn1 = By.id("ua.com.rozetka.shop:id/b_main");
    private By main_tv_to_catalog = By.xpath("//android.widget.TextView[@text='Каталог']");
    private By main_to_notebooks = By.xpath("//android.widget.TextView[@text='Ноутбуки и компьютеры']");
    private By main_to_notebooks1 = By.xpath("//android.widget.TextView[@text='Ноутбуки']");
    private By menuAll = By.xpath("//android.widget.TextView[@text='ВСЕ']");
    private By asusNotebook = By.xpath("//android.widget.TextView[@text='Ноутбук Asus X509FL-BQ053 (90NB0N12-M01880) Slate Grey Суперцена!!!']");
    private By addToCart = By.xpath("//android.widget.TextView[@text='В КОРЗИНУ']");


    public CatalogPage(Driver driver) {
        super(driver);
    }

    public CatalogPage clickEnter() {
        getElementBy(smileRozetka).click();
        logger.info("Click by Rozetka Smile Button.");
        return this;
    }

    public CatalogPage clickImageBucket() {
        getElementBy(iv_toolbar_cart).click();
        logger.info("Click by ImavgeView on Toolbar Cart Button.");
        return this;
    }

    public CatalogPage clickMainBtn() {
        getElementBy(mainBtn1).click();
        logger.info("Click by MainButton on Page Cart.");
        return this;
    }

    public CatalogPage clickToCatalog() {
        getElementBy(main_tv_to_catalog).click();
        logger.info("Click by main_tv_to_catalog.");
        return this;
    }

    public CatalogPage clickMenuNotebook() {
        getElementBy(main_to_notebooks).click();
        logger.info("Click by main_to_notebooks.");
        return this;
    }

    public CatalogPage clickMenuNotebookStep1() {
        getElementBy(main_to_notebooks1).click();
        logger.info("Click by main_to_notebooks1.");
        return this;
    }

    public CatalogPage clickMenuAll() {
        getElementBy(menuAll).click();
        logger.info("Click by clickMenuAll.");
        return this;
    }

    public CatalogPage chooseAsusNotebook() {
        getElementBy(asusNotebook).click();
        logger.info("Click by asusNotebook.");
        return this;
    }

    public CatalogPage addToCart() {
        getElementBy(addToCart).click();
        logger.info("Click by addToCart.");
        return this;
    }

}
