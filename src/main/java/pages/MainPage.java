package pages;

import core.BaseElement;
import core.BasePage;
import core.Driver;
import exception.MobileException;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MainPage extends BasePage {

    private By findText = By.xpath("//android.widget.EditText[@text='Я ищу…']");
    private By el1 = By.xpath("//android.widget.TextView[@instance='11']");
    private By el2 = By.xpath("//android.widget.TextView[@instance='5']");
    private By firstLeftProduct = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.FrameLayout");
    private By buyButtonOnProductPage = By.id("ua.com.rozetka.shop:id/bottom_bar_tv_buy");
    private By backButtonMoveFromAuthPageToCart = By.xpath("//android.widget.ImageButton[@content-desc=\"Перейти вверх\"]");
    private By productFirstInCart = By.id("ua.com.rozetka.shop:id/iv_cart_offer_item_offer");
    private By optionCartButton = By.xpath("//android.widget.ImageView[@content-desc=\"Ещё\"]\n");
    private By deleteButtonInOption = By.xpath("//android.widget.TextView[@text='Удалить из корзины']");
    private By addToWishListButton = By.id("ua.com.rozetka.shop:id/bottom_bar_iv_wish_image");
    private By leftMenuOpen = By.xpath("//android.widget.ImageButton[@content-desc=\"Перейти вверх\"]\n");
    private By wishListInLeftMenu = By.xpath("//android.widget.TextView[@text='Список желаний']");
    private By productFirstInWishList = By.id("ua.com.rozetka.shop:id/tv_title");
    private By optionProductinWishList = By.id("ua.com.rozetka.shop:id/iv_menu");
    private By deleteButtoninOptionProductWishList = By.xpath("//android.widget.TextView[@text='Удалить товар']");

    public MainPage(Driver driver) {
        super(driver);
    }


    public MainPage clickOnFirstLeftProduct() {
        logger.info("in clickOnFirstLeftProduct()");
        getElementBy(firstLeftProduct).click();
        return this;
    }

    public MainPage clickOnProductFirstInWishList() {
        logger.info("in clickOnProductFirstInWishList()");
        getElementBy(productFirstInWishList).click();
        return this;
    }


    public MainPage clickOnDeleteButtoninOptionProductWishList() {
        logger.info("in clickOnDeleteButtoninOptionProductWishList()");
        getElementBy(deleteButtoninOptionProductWishList).click();
        return this;
    }


    public MainPage clickOnOptionProductinWishList() {
        logger.info("in clickOnOptionProductinWishList()");
        getElementBy(optionProductinWishList).click();
        return this;
    }


    public MainPage clickOnWishListInLeftMenu() {
        logger.info("in clickOnWishListInLeftMenu()");
        getElementBy(wishListInLeftMenu).click();
        return this;
    }


    public MainPage clickOnLeftMenuOpen() {
        logger.info("in clickOnLeftMenuOpen()");
        getElementBy(leftMenuOpen).click();
        return this;
    }

    public MainPage clickOnAddToWishListButton() {
        logger.info("in clickOnAddToWishListButton()");
        getElementBy(addToWishListButton).click();
        return this;
    }

    public MainPage clickOnBuyButtonOnProductPage() {
        logger.info("in clickOnBuyButtonOnProductPage()");
        getElementBy(buyButtonOnProductPage).click();
        return this;
    }

    public MainPage clickAndMoveFromAuthPAgeToCart() {
        logger.info("in clickAndMoveFromAuthPAgeToCart()");
        getElementBy(backButtonMoveFromAuthPageToCart).click();
        return this;
    }

    public MainPage verifyAnyProductInCart() {
        logger.info("in verifyAnyProductInCart()");
        assertTrue(getElementBy(productFirstInCart).isExists());
        return this;
    }


    public MainPage verifyAnyProductInWishList() {
        logger.info("in verifyAnyProductInWishList()");
        assertTrue(getElementBy(productFirstInWishList).isExists());
        return this;
    }

    public MainPage verifyAnyProductNotInCart() {
        logger.info("in verifyAnyProductNotInCart()");
        try {
            assertFalse(getElementBy(productFirstInCart).isExists());
            logger.info("MobileException in verifyAnyProductNotInCart() not cached, element not on page. Test failed");
        } catch (MobileException e) {
            logger.info("Catch MobileException in verifyAnyProductNotInCart(), element not on page. Test passed");
            assertFalse(false);
        }
        return this;
    }

    public MainPage verifyAnyProductNotInWishList() {
        logger.info("in verifyAnyProductNotInWishList()");
        try {
            assertFalse(getElementBy(productFirstInWishList).isExists());
            logger.info("MobileException in verifyAnyProductNotInWishList() not cached, element not on page. Test failed");
        } catch (MobileException e) {
            logger.info("Catch MobileException in verifyAnyProductNotInWishList(), element not on page. Test passed");
            assertFalse(false);
        }
        return this;

    }

    public MainPage clickOptionProductInCart() {
        logger.info("in clickOptionProductInCart()");
        getElementBy(optionCartButton).click();
        return this;
    }

    public MainPage clickDeleteButtonInOption() {
        logger.info("in clickDeleteButtonInOption()");
        getElementBy(deleteButtonInOption).click();
        return this;
    }

    public MainPage clickOnCatalogButton() {
        BaseElement back = getElementBy(By.id("ua.com.rozetka.shop:id/main_tv_to_catalog"));
        back.click();
        return this;
    }

    public MainPage clickOnSearchField() {
        BaseElement search = getElementBy(By.id("ua.com.rozetka.shop:id/search_iv_back"));
        search.click();
        return this;
    }

    public MainPage sendSearchText() {
        BaseElement searchField = getElementBy(findText);
        searchField.sendKeys("asus");
        return this;
    }

    public MainPage testSwipe() {

        TouchAction act = new TouchAction(driver.getDriver());
        Point el1coordinates = driver.getDriver().findElement(el1).getLocation();
        Point el2coordinates = driver.getDriver().findElement(el2).getLocation();
        act.press(PointOption.point(el1coordinates.x, el1coordinates.y))
                .moveTo(PointOption.point((el2coordinates.x - el1coordinates.x), (el2coordinates.y - el1coordinates.y)))
                .release().perform();
        return this;

    }
}
