package pages;

import core.BasePage;
import core.Driver;
import org.openqa.selenium.By;

import static org.junit.Assert.assertTrue;

public class ProductPage extends BasePage {

    private By catalogBtn = By.id("ua.com.rozetka.shop:id/main_tv_to_catalog");
    private By menuItemHeader = By.xpath("//android.widget.ImageView[@instance='1']");
    private By menuLaptopesAndComputers = By.xpath("//android.widget.TextView[@instance='2']");
    private By menuTablets = By.xpath("//android.widget.TextView[@instance='4']");
    private By menuBudgetTablets = By.xpath("//android.widget.ImageView[@instance='1']");
    private By filterBtn = By.xpath("//android.widget.TextView[@instance='4']");
    private By usedItemsCheckbox = By.xpath("//android.widget.CheckBox[@index='3']");
    private By filterApplyBtn = By.id("ua.com.rozetka.shop:id/view_filters_tv_apply");
    private By sortBtn = By.xpath("//android.widget.TextView[@instance='2']");
    private By firstRadioBtn = By.xpath("//android.widget.RadioButton[@instance='0']");
    private By displayTypeBtn = By.id("ua.com.rozetka.shop:id/iv_display_type");
    private By firstListProduct = By.xpath("//android.widget.ImageView[@instance='5']");
    private By keepTrackPrice = By.id("ua.com.rozetka.shop:id/offer_iv_price_change");
    private By loginField = By.id("ua.com.rozetka.shop:id/sign_in_et_login");
    private By passwordField = By.id("ua.com.rozetka.shop:id/sign_in_et_password");
    private By signInBtn = By.id("ua.com.rozetka.shop:id/sign_in_tv_login");
    private By productInWaitList = By.id("ua.com.rozetka.shop:id/iv_image");


    public ProductPage(Driver driver) {
        super(driver);
    }

    public ProductPage clickMenuItemHeader() {
        getElementBy(menuItemHeader).click();
        return this;
    }

    public ProductPage clickCatalogBtn() {
        wait(2000);
        getElementBy(catalogBtn).click();
        return this;
    }

    public ProductPage clickMenuLaptopesAndComputers() {
        getElementBy(menuLaptopesAndComputers).click();
        return this;
    }

    public ProductPage clickMenuTablets() {
        getElementBy(menuTablets).click();
        return this;
    }

    public ProductPage clickMenuBudgetTablets() {
        getElementBy(menuBudgetTablets).click();
        return this;
    }

    public ProductPage clickFilterBtn() {
        getElementBy(filterBtn).click();
        wait(10000);
        return this;
    }

    public ProductPage clickUsedItemsCheckbox() {
        getElementBy(usedItemsCheckbox).click();
        wait(5000);
        return this;
    }

    public ProductPage clickFilterApplyBtn() {
        getElementBy(filterApplyBtn).click();
        return this;
    }

    public ProductPage clickSortBtn() {
        getElementBy(sortBtn).click();
        return this;
    }

    public ProductPage clickFirstRadioBtn() {
        getElementBy(firstRadioBtn).click();
        return this;
    }

    public ProductPage clickDisplayTypeBtn() {
        getElementBy(displayTypeBtn).click();
        return this;
    }

    public ProductPage clickFirstListProduct() {
        getElementBy(firstListProduct).click();
        return this;
    }

    public ProductPage clickKeepTrackPrice() {
        getElementBy(keepTrackPrice).click();
        return this;
    }

    public ProductPage fillLoginField(String s) {
        getElementBy(loginField).sendKeys(s);
        return this;
    }

    public ProductPage fillPasswordField(String s) {
        getElementBy(passwordField).sendKeys(s);
        return this;
    }

    public ProductPage clickSignInBtn() {
        getElementBy(signInBtn).click();
        return this;
    }

    public ProductPage hideKeyboard() {
        driver.getDriver().hideKeyboard();
        return this;
    }

    public ProductPage verifyProductInWaitList() {
        assertTrue("Element isn't exist on the page", getElementBy(productInWaitList).isExists());
        return this;
    }
}