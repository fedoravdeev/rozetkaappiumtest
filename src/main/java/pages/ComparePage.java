package pages;

import core.BasePage;
import core.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;

public class ComparePage extends BasePage {

    private By rozetkaLabel = By.id("ua.com.rozetka.shop:id/ll_search");
    private By laptopsBtn = By.xpath("//android.widget.TextView[@instance='3']");
    private By prestigioSmartBook = By.xpath("//android.widget.ImageView[@instance='5']");
    private By sravnitBtn1 = By.xpath("//android.widget.ImageView[@instance='5']");
    private By laptopBackBtn = By.xpath("//android.widget.TextView[@instance='0']");
    private By asusX509FL = By.xpath("//android.widget.RelativeLayout[@instance='6']");
    private By sravnitBtn2 = By.xpath("//android.widget.ImageView[@instance='7']");
    private By menuBtn = By.xpath("//android.widget.ImageButton[@instance='0']");
    private By compareBtn = By.xpath("//android.widget.TextView[@instance='19']");
    private By elementsCompare = By.id("ua.com.rozetka.shop:id/comparisons_item_rv_comparisons_images");
    private By difference = By.id("ua.com.rozetka.shop:id/comparison_tv_diff_title");
    private By moreBtn = By.id("ua.com.rozetka.shop:id/comparison_ll_add_more_layout");
    private By lenovoPad = By.xpath("//android.widget.RelativeLayout[@instance='8']");
    private By sravnitBtn3 = By.xpath("//android.widget.ImageView[@instance='5']");
    private By getLeftLaptop = By.xpath("//android.widget.TextView[@text='Ноутбук Prestigio SmartBook 141 С3 (PSB141C03_BFH) Dark Gray Суперцена!!!']");
    private By getMiddleLaptop = By.xpath("//android.widget.TextView[@text='Ноутбук Asus X509FL-BQ053 (90NB0N12-M01880) Slate Grey Суперцена!!!']");
    private By getRightLaptop = By.xpath("//android.widget.TextView[@text='Ноутбук Lenovo IdeaPad 330-15AST (81D600M0RA) Onyx Black Суперцена!!!']");

    public ComparePage(Driver driver) {
        super(driver);
    }

    public ComparePage clickRozetkaBtn() {
        getElementBy(rozetkaLabel).click();
        return this;
    }

    public ComparePage clickLaptops() {
        wait(5);
        getElementBy(laptopsBtn).click();
        return this;
    }

    public ComparePage clickPrestigioItem() {
        wait(2000);
        getElementBy(prestigioSmartBook).click();
        return this;
    }

    public ComparePage clickCmpFirstBtn() {
        wait(2000);
        getElementBy(sravnitBtn1).click();
        return this;
    }

    public ComparePage clickBackBtn() {
        wait(2000);
        getElementBy(laptopBackBtn).click();
        return this;
    }

    public ComparePage clickAsusItem() {
        wait(2000);
        getElementBy(asusX509FL).click();
        return this;
    }

    public ComparePage clickCmpSecondBtn() {
        wait(2000);
        getElementBy(sravnitBtn2).click();
        return this;
    }

    public ComparePage clickMenu() {
        wait(2000);
        getElementBy(menuBtn).click();
        return this;
    }

    public ComparePage clickCompareMenuItem() {
        wait(2000);
        getElementBy(compareBtn).click();
        return this;
    }

    public ComparePage clickElementsCompare() {
        wait(2000);
        getElementBy(elementsCompare).click();
        return this;
    }

    public ComparePage clickDifference() {
        wait(2000);
        getElementBy(difference).click();
        return this;
    }

    public ComparePage scrollVertical() {
        swipeToBottom();
        return this;
    }

    public ComparePage scrollHorizontal() {
        wait(2000);
        horizontalSwipeByPercentage(0.8, 0.3, 0.8);
        return this;
    }

    public ComparePage clickMoreBtn() {
        wait(2000);
        getElementBy(moreBtn).click();
        return this;
    }

    public ComparePage clickLenovoPadItem() {
        wait(2000);
        getElementBy(lenovoPad).click();
        return this;
    }

    public ComparePage clickCmpLenovoPad() {
        wait(2000);
        getElementBy(sravnitBtn3).click();
        return this;
    }

    public ComparePage verifyLeftElement() {
        wait(2000);
        Assert.assertTrue(getElementBy(getLeftLaptop).isExists());
        return this;
    }

    public ComparePage verifyMiddleElement() {
        wait(2000);
        Assert.assertTrue(getElementBy(getMiddleLaptop).isExists());
        return this;
    }

    public ComparePage verifyRightElement() {
        Assert.assertTrue(getElementBy(getRightLaptop).isExists());
        return this;
    }

}
