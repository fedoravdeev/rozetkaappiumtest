package pages;

import core.BasePage;
import core.Driver;
import org.openqa.selenium.By;

import static org.junit.Assert.assertTrue;

public class PersonalAccountPage extends BasePage {

    private By personalAccountMenu = By.xpath("//android.widget.TextView[@instance='10']");
    private By loginField = By.id("ua.com.rozetka.shop:id/sign_in_et_login");
    private By passwordField = By.id("ua.com.rozetka.shop:id/sign_in_et_password");
    private By signInBtn = By.id("ua.com.rozetka.shop:id/sign_in_tv_login");
    private By editBtn = By.id("ua.com.rozetka.shop:id/action_edit");
    private By yogaCheckbox = By.xpath("//android.widget.CheckBox[@instance='11']");
    private By submitBtn = By.id("ua.com.rozetka.shop:id/action_done");
    private By yogaInfo = By.xpath("//android.widget.TextView[@instance='15']");


    public PersonalAccountPage(Driver driver) {
        super(driver);
    }

    public PersonalAccountPage clickPersonalAccountMenu() {
        getElementBy(personalAccountMenu).click();
        return this;
    }

    public PersonalAccountPage fillLoginField(String s) {
        getElementBy(loginField).sendKeys(s);
        return this;
    }

    public PersonalAccountPage fillPasswordField(String s) {
        getElementBy(passwordField).sendKeys(s);
        return this;
    }

    public PersonalAccountPage hideKeyboard() {
        driver.getDriver().hideKeyboard();
        return this;
    }

    public PersonalAccountPage clickSignInBtn() {
        getElementBy(signInBtn).click();
        return this;
    }

    public PersonalAccountPage clickEditBtn() {
        getElementBy(editBtn).click();
        return this;
    }

    public PersonalAccountPage clickYogaCheckbox() {
        getElementBy(yogaCheckbox).click();
        return this;
    }

    public PersonalAccountPage clickSubmitBtn() {
        getElementBy(submitBtn).click();
        return this;
    }

    public PersonalAccountPage verifyYogaInfo() {
        assertTrue("Element isn't exist on the page", getElementBy(yogaInfo).isExists());
        return this;
    }
}
