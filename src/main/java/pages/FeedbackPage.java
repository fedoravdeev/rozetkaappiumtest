package pages;

import core.BasePage;
import core.Driver;
import org.openqa.selenium.By;

import static org.junit.Assert.assertFalse;

public class FeedbackPage extends BasePage {

    private By feedbackMenu = By.xpath("//android.widget.TextView[@instance='11']");
    private By secondRadioBtn = By.xpath("//android.widget.RadioButton[@instance='1']");
    private By otherQuestsRadioBtn = By.xpath("//android.widget.RadioButton[@instance='6']");
    private By messageField = By.id("ua.com.rozetka.shop:id/et_feedback_message");
    private By nameField = By.id("ua.com.rozetka.shop:id/et_feedback_name");
    private By emailField = By.id("ua.com.rozetka.shop:id/et_feedback_email");
    private By sendBtn = By.id("ua.com.rozetka.shop:id/material_button_tv_text");
    private static final String messageSuccess = "Ваше обращение принято";

    public FeedbackPage(Driver driver) {
        super(driver);
    }

    public FeedbackPage clickFeedbackMenu() {
        getElementBy(feedbackMenu).click();
        return this;
    }

    public FeedbackPage clickSecondRadioBtn() {
        wait(2000);
        getElementBy(secondRadioBtn).click();
        return this;
    }

    public FeedbackPage clickOtherQuestsRadioBtn() {
        wait(2000);
        getElementBy(otherQuestsRadioBtn).click();
        return this;
    }

    public FeedbackPage fillMessageField(String s) {
        getElementBy(messageField).sendKeys(s);
        return this;
    }

    public FeedbackPage hideKeyboard() {
        driver.getDriver().hideKeyboard();
        return this;
    }

    public FeedbackPage fillNameField(String s) {
        getElementBy(nameField).sendKeys(s);
        return this;
    }

    public FeedbackPage fillEmailField(String s) {
        getElementBy(emailField).sendKeys(s);
        return this;
    }

    public FeedbackPage clickSendBtn() {
        getElementBy(sendBtn).click();
        return this;
    }

    public FeedbackPage verifyMessageSuccess() {
        assertFalse("Success message exists on the page", Boolean.parseBoolean(messageSuccess));
        return this;
    }
}