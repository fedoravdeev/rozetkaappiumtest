package exception;

import org.apache.log4j.Logger;

public class MobileException extends RuntimeException {
    private static final Logger logger = Logger.getRootLogger();
    public MobileException(String s) {
        super(s);
    }

    public MobileException(String message, Throwable cause) {
        super(message, cause);
        logger.error(message,cause);
    }
}
